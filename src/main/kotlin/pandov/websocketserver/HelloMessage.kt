package pandov.websocketserver

class HelloMessage {
    lateinit var name: String

    constructor()

    constructor(name: String) {
        this.name = name
    }
}